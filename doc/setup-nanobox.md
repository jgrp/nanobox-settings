# Setup Nanobox on Vagrant box

- https://docs.nanobox.io/install/
- https://docs.docker.com/install/linux/docker-ce/ubuntu/#extra-steps-for-aufs
- https://docs.docker.com/install/linux/linux-postinstall/
- install a package: `sudo dpkg -i DEB_PACKAGE`
- https://guides.nanobox.io/php/generic/from-scratch/